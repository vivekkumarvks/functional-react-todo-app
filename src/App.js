import "./App.css";
import Header from "./components/Header";
import TodoForm from "./components/TodoForm";
import Filter from "./components/Filter";
import TodoList from "./components/TodoList";
import "./components/component.css";
import { useReducer } from "react";

const getTodoList = () => {
  return JSON.parse(localStorage.getItem("todo_list"));
};

const setTodoList = (listData) => {
  localStorage.setItem("todo_list", JSON.stringify(listData));
};

let initialState = getTodoList() || [];

const handleTaskReducer = (state, action) => {
  switch (action.type) {
    case "add":
      setTodoList([action.data, ...state]);
      return [action.data, ...state];

    case "remove":
      let removedTodoList = state.filter((element) => {
        return element.id !== action.id;
      });

      setTodoList(removedTodoList);
      return removedTodoList;

    case "filter":
      let todoList = getTodoList();

      if (action.filterBy !== "all") {
        todoList = todoList.filter((data) => {
          return data.completed === action.filterBy;
        });
      }

      return todoList;

    case "done":
      let todo_list = state.map((element) => {
        if (element.id === action.index) {
          element.completed = true;
        }
        return element;
      });

      setTodoList(todo_list);
      return todo_list;

    case "clearAll":
      setTodoList([]);
      return [];

    default:
      return initialState;
  }
};

function App() {
  let [todoList, handleTodoList] = useReducer(handleTaskReducer, initialState);

  const removeTask = (id) => {
    handleTodoList({ type: "remove", id });
  };

  const addTask = (task) => {
    handleTodoList({ type: "add", data: task });
  };

  const filter = (filterBy) => {
    handleTodoList({ type: "filter", filterBy });
  };

  const markAsDone = (index) => {
    handleTodoList({ type: "done", index });
  };

  const clearAll = () => {
    handleTodoList({ type: "clearAll" });
  };

  return (
    <div className="container">
      <Header clearAll={clearAll} />
      <TodoForm handleTodoForm={addTask} />
      <Filter filter={filter} />
      <TodoList
        list={todoList}
        removeTask={removeTask}
        markAsDone={markAsDone}
      />
    </div>
  );
}

export default App;
