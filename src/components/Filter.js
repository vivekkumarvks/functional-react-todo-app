import React from "react";

function Filter(props) {
  return (
    <div className="block filter">
      <button
        className="all-button filter-button"
        onClick={() => props.filter("all")}
        type="button"
      >
        All
      </button>
      <button className="filter-button" onClick={() => props.filter(true)}>
        Completed
      </button>
      <button className="filter-button" onClick={() => props.filter(false)}>
        UnCompleted
      </button>
    </div>
  );
}

export default Filter;
