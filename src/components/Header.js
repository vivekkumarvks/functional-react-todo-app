import React from "react";
import "./component.css";

function Header(props) {
  return (
    <div className="header">
      <h1>Todoey!</h1>
      <button className="clear-all" onClick={props.clearAll}>
        Clear All
      </button>
    </div>
  );
}

export default Header;
